package setupRequest;

import endpoints.EndpointInterface;
import org.apache.jmeter.samplers.SampleResult;

public class EndpointCaller {

    public SampleResult request (EndpointInterface endpoint) {
        return endpoint.getRequest().sample();
    }
}
