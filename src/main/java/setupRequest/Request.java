package setupRequest;

import org.apache.jmeter.protocol.http.control.Header;
import org.apache.jmeter.protocol.http.control.HeaderManager;
import org.apache.jmeter.protocol.http.sampler.HTTPSampler;
import org.apache.jmeter.samplers.SampleResult;

public class Request {

    private HTTPSampler httpSampler = new HTTPSampler();
    private HeaderManager headerManager;

    public Request setProtocol(String protocol) {
        httpSampler.setProtocol(protocol);

        return this;
    }

    public Request setDomain(String domain) {
        httpSampler.setDomain(domain);

        return this;
    }

    public Request setPath(String path) {
        httpSampler.setPath(path);

        return this;
    }

    public Request setMethod(String method) {
        httpSampler.setMethod(method);

        return this;
    }

    public Request addHeader(Header header) {
        if (headerManager == null) {
            headerManager = new HeaderManager();
        }
        headerManager.add(header);
        return this;
    }

    public Request addNonEncodedArgument(String value) {
        httpSampler.addNonEncodedArgument("", value, "");

        return this;
    }

    public Request setPostBodyRaw(Boolean bodyRow) {
        httpSampler.setPostBodyRaw(bodyRow);

        return this;
    }

    public SampleResult sample() {
        if (headerManager != null) {
            httpSampler.setHeaderManager(headerManager);
        }
        return httpSampler.sample();
    }
}
