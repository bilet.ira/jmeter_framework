package configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.text.MessageFormat;
import java.util.Properties;

public class PropertyReader {

    private final Properties properties;
    private final String filePath = "src/main/resources/system.properties";

    public PropertyReader() {
        properties = new Properties();
        try {
            properties.load(new FileInputStream(filePath));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getProperty(String key) {
        if (properties.isEmpty() || properties.getProperty(key) == null) {
            throw new InvalidParameterException(MessageFormat
                    .format("Either Key or Key value is Missing for key --[" + key + "] in configuration file.", key));
        } else
            return properties.getProperty(key);
    }
}
