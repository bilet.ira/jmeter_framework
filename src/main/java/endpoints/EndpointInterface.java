package endpoints;

import setupRequest.Request;

public interface EndpointInterface {

    Request getRequest();
}
