package endpoints;

import configuration.PropertyReader;
import org.apache.jmeter.protocol.http.control.Header;
import setupRequest.Request;

public class AuthorizationEndpoint implements EndpointInterface {

    private PropertyReader propertyReader = new PropertyReader();
    private String data;

    public AuthorizationEndpoint(String data) {
        this.data = data;
    }

    public Request getRequest() {
        return (new Request())
                .setMethod("POST")
                .setProtocol(propertyReader.getProperty("salesForceProtocol"))
                .setDomain(propertyReader.getProperty("salesForceDomain"))
                .setPath("/services/Soap/u/44.0")
                .addHeader(new Header("Content-Type", "text/xml"))
                .addHeader(new Header("SOAPAction", "login"))
                .addNonEncodedArgument(data)
                .setPostBodyRaw(true);
    }
}
