package endpoints;

import setupRequest.Request;

public class CreateNewCaseEndpoint extends EndpointParameter {

    public CreateNewCaseEndpoint(String data, String token) {
        super(data, token);
    }

    public Request getRequest() {
        return super.getRequest()
                .setMethod("POST")
                .setPath("/services/apexrest/IVPCI/v1/Intake");
    }
}
