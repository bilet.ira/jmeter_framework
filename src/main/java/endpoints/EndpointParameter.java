package endpoints;

import configuration.PropertyReader;
import org.apache.jmeter.protocol.http.control.Header;
import setupRequest.Request;

public class EndpointParameter implements EndpointInterface {

    private PropertyReader propertyReader = new PropertyReader();
    private String data;
    private String token;

    EndpointParameter(String data, String token) {
        this.data = data;
        this.token = token;
    }

    public Request getRequest() {
        return (new Request())
                .setProtocol(propertyReader.getProperty("caseIntakeProtocol"))
                .setDomain(propertyReader.getProperty("caseIntakeDomain"))
                .addHeader(new Header("Content-Type", "application/json"))
                .addHeader(new Header("Authorization", "Bearer " + token))
                .addNonEncodedArgument(data)
                .setPostBodyRaw(true);
    }
}

