package fileData.dataReader;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;

public class XMLReader {

    private static final String loginPath = "src/main/java/fileData/xml/login.xml";
    private static final String sessionIdXPath = "//sessionId/text()";

    public static StringBuilder xmlReader () throws IOException {
        File xmlFile = new File(loginPath);

        Reader fileReader = new FileReader(xmlFile);
        BufferedReader bufReader = new BufferedReader(fileReader);

        StringBuilder sb = new StringBuilder();
        String line = bufReader.readLine();
        while (line != null) {
            sb.append(line).append("\n");
            line = bufReader.readLine();
        }
        return sb;
    }

    public static String getSessionIdFromXml(String xml) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        InputSource inputXML = new InputSource(new StringReader(xml));

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.parse(inputXML);

        XPathFactory xpathFactory = XPathFactory.newInstance();
        XPath xpath = xpathFactory.newXPath();

        String result = xpath.evaluate(sessionIdXPath, document);

        if (result == null) {
            throw new IOException("Session id is not found");
        }

        return result;
    }
}
