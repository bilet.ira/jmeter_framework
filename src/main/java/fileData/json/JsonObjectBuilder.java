package fileData.json;

import org.json.simple.JSONObject;

public class JsonObjectBuilder {
    private JSONObject jsonObject = new JSONObject();

    public JsonObjectBuilder add(String key, String value) {
        jsonObject.put(key, value);

        return this;
    }

    public JsonObjectBuilder add(String key, int value) {
        jsonObject.put(key, value);

        return this;
    }

    public JsonObjectBuilder add(String key, Boolean value) {
        jsonObject.put(key, value);

        return this;
    }

    public JsonObjectBuilder add(String key, JsonArrayBuilder value) {
        jsonObject.put(key, value.build());

        return this;
    }

    public JSONObject build() {
        return jsonObject;
    }
}
