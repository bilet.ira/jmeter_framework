package fileData.json;

import org.apache.commons.lang3.RandomUtils;

public class createNewCaseRecordBuilder implements BuilderInterface {

    @Override
    public String build() {
        JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder();

        jsonObjectBuilder
            .add("requestType", "Create_Intake_Items")
            .add("cases", new JsonArrayBuilder()
                .add(new JsonObjectBuilder()
                    .add("extId", generateRandomID())
                    .add("sourceDocuments", new JsonArrayBuilder()
                        .add(
                            new JsonObjectBuilder()
                                .add("storageFileSource", "Email Message")
                                .add("storageFileId", generateRandomID())
                                .add("storageFileName", "file_A.pdf")
                                .add("storageFileType", "pdf")
                                .add("emailFromAddress", "customerA@mail.iqvia.com")
                                .add("emailSubject", "CASE #3245/02")
                                .add("emailDateTimeReceived", "2016-02-28T16:41:41.090Z")
                                .add("emailHasSubject", true)
                                .add("emailHasBody", true)
                                .add("emailHasAttachment", true)
                                .add("sourceDocuments", new JsonArrayBuilder()
                                    .add(new JsonObjectBuilder()
                                        .add("storageFileSource", "Email Attachment")
                                        .add("storageFileId", generateRandomID())
                                        .add("storageFileName", "case001.pdf")
                                        .add("storageFileType", "pdf")
                                        .add("emailFromAddress", "customerA@mail.iqvia.com")
                                        .add("emailDateTimeReceived", "2016-02-28T16:41:41.090Z")
                                    )
                                )
                        )
                    )
                )
            );

        return jsonObjectBuilder.build().toString();
    }

    private int generateRandomID() {
        return Math.abs(RandomUtils.nextInt(1,999999) + (int) System.currentTimeMillis());
    }

}
