package fileData.json;

public interface BuilderInterface {
    String build();
}
