package fileData.json;

import org.json.simple.JSONArray;

public class JsonArrayBuilder {
    private JSONArray JsonArray = new JSONArray();

    public JsonArrayBuilder add(JsonObjectBuilder value) {
        JsonArray.add(value.build());

        return this;
    }

    public JSONArray build()
    {
        return JsonArray;
    }
}
