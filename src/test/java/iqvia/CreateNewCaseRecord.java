package iqvia;

import endpoints.CreateNewCaseEndpoint;
import fileData.json.BuilderInterface;
import fileData.json.createNewCaseRecordBuilder;
import org.apache.jmeter.samplers.SampleResult;
import org.testng.annotations.Test;

public class CreateNewCaseRecord extends BaseTest {

    @Test
    public void createNewCaseRecord() {
        BuilderInterface builder = new createNewCaseRecordBuilder();
        String data = builder.build();
        SampleResult response = endpointCaller.request(new CreateNewCaseEndpoint(data, token));

        System.out.println(response.getResponseHeaders());
        System.out.println(data);
        System.out.println(response.getResponseDataAsString());
    }
}
