package iqvia;

import configuration.PropertyReader;
import endpoints.AuthorizationEndpoint;
import fileData.dataReader.XMLReader;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.util.JMeterUtils;
import org.testng.annotations.BeforeTest;
import org.xml.sax.SAXException;
import setupRequest.EndpointCaller;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;

public class BaseTest {
    protected EndpointCaller endpointCaller = new EndpointCaller();
    protected String token;

    @BeforeTest
    public void setupJMeter() throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        StringBuilder xml = XMLReader.xmlReader();
        SampleResult response = endpointCaller.request(new AuthorizationEndpoint(xml.toString()));
        token = XMLReader.getSessionIdFromXml(response.getResponseDataAsString());

        PropertyReader propertyReader = new PropertyReader();
        String jMeterHome = propertyReader.getProperty("jMeterHomePath");
        JMeterUtils.setJMeterHome(jMeterHome);
        JMeterUtils.loadJMeterProperties(jMeterHome + propertyReader.getProperty("jMeterPropertiesPath"));
        JMeterUtils.initLocale();
    }
}
